//
//  ContentView.swift
//  Task4
//
//  Created by Aditya Khadke on 29/04/22.
//

import SwiftUI
import Alamofire
import Shimmer

struct ContentView: View {
    
    @State var schooldata : Schools?
    @State var isLoading = true
    var body: some View {
        List(0..<11) { i in
            HStack {
                Text(schooldata?.data[i].name ?? " ")
                    .frame(width: 200, height: 30, alignment: .leading)
                    .multilineTextAlignment(.leading)
                    .lineLimit(5)
                    Spacer()
                    
                Text(schooldata?.data[i].nation ?? " ")
                    .frame(width: 40, height: 20, alignment: .leading)
            }
            .padding()
            .font(.system(size: 15, weight: .semibold, design: .rounded))
            .redacted(reason: isLoading ? .placeholder : [])
            .shimmering(active: isLoading ? true : false)
            .onAppear{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    loadData()
                }
            }
            
            
            
            
            
            
        }
        
        
        
        
    }
    
    
    func loadData() {

        AF
            .request("https://staging.instapreps.com/api/schools")
            .validate(statusCode: 200..<400)
            .responseDecodable(of: Schools.self) { response in
                switch response.result {
                case .success(let items):
                    
                    print("items are" , items )
                    self.schooldata = items
                    
                    
                case .failure(let error):
                    print("Error" , error.localizedDescription)
                }
                
                isLoading = false
                
                
                
            }
    }
    
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
