//
//  Task4App.swift
//  Task4
//
//  Created by Aditya Khadke on 29/04/22.
//

import SwiftUI

@main
struct Task4App: App {
    var body: some Scene {
        WindowGroup {
            CardView()
        }
    }
}
