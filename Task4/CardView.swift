//
//  CardView.swift
//  Task4
//
//  Created by Aditya Khadke on 05/05/22.
//

import SwiftUI

struct CardView: View {
    @State var it = items
    var body: some View {
        ZStack {
            
            Color.black.opacity(0.01)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                VStack(alignment: .leading, spacing: 10.0) {
                    Text("Students and Parents Trust Us")
                        .font(.system(size: 26, weight: .bold, design: .rounded))
                        .foregroundColor(.indigo)
                        .multilineTextAlignment(.leading)
                    
                    Text("We help them Study the confidence way")
                        .font(.subheadline)
                    
                }
                Spacer()
            }
            
            
            
            ScrollViewReader { proxy in
            ScrollView(.horizontal , showsIndicators: false) {
                HStack {
                    
                    ForEach(it.indices , id: \.self) { index in
        
                        
                        MenuCard(cardi: self.it[index])
                            
                            
                        
                        
                    }
                }
               
                
            }
            .onAppear{
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    withAnimation(Animation.easeInOut(duration: 0.3)) {
                        proxy.scrollTo(it.startIndex + 2)

                    }
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    
                    withAnimation(Animation.easeInOut(duration: 0.3)) {
                        proxy.scrollTo(it.startIndex + 3)

                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                    withAnimation(Animation.easeInOut(duration: 0.3)) {
                        proxy.scrollTo(it.startIndex)

                    }
                

                    
                    
                }
                
            
            }
                
        }
            
            .padding(.leading,5)
            
            .offset(y:-200)
            
            
            
            
        }
        
        
        
        
        
    }
    
    
}
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView()
    }
}

struct MenuCard: View {
    var cardi : CardItems
    var body: some View {
        HStack {
            VStack {
                Image(cardi.image)
                
                VStack(spacing: 15.0) {
                    Text(cardi.mainText)
                        .font(.system(size: 24, weight: .bold, design: .rounded))
                        .foregroundColor(.indigo)
                    
                    Text(cardi.subText)
                        .font(.system(size: 15, weight: .light, design: .rounded))
                        .padding(.horizontal)
                }
                
                
            }
            .padding()
            
            .frame(width: 200, height: 250, alignment: .center)
            
            .background(Color.black.opacity(0.1))
            .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
        }
    }
}


struct CardItems: Identifiable {
    var id = UUID()
    var mainText: String
    var subText: String
    var image: String
}


let items = [ CardItems(mainText: "2.3K+", subText: "InstaPrep Locations", image: "map"),
              CardItems(mainText: "1.2 Lacs+", subText: "Confidence Diagnosed", image: "card"),
              CardItems(mainText: "6 Lacs+", subText: "Total No. of questions solved", image: "contacts"),
              CardItems(mainText: "4.5 Stars", subText: "Playstore Rating", image: "stars")
              
              
              
              
]
