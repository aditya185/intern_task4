//
//  Test.swift
//  Task4
//
//  Created by Aditya Khadke on 02/05/22.
//

import SwiftUI

struct Test: View {
    @State var loading = false
    var body: some View {
        HStack {
            Text("Hello World")
            Spacer()
            Text("Hello Aditya")
        }
        .frame(width: 400, height: 50, alignment: .center)
        .padding()
        .redacted(reason: loading ? .placeholder : [])
        .shimmering(active: loading ? true : false)
        .onAppear(perform: isLoading)
        
    }
        
        
    
    func isLoading() {
        loading = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            loading = false
        }
    }
}


struct Test_Previews: PreviewProvider {
    static var previews: some View {
        Test()
    }
}
