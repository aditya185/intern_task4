//
//  SchoolData.swift
//  Task4
//
//  Created by Aditya Khadke on 29/04/22.
//

import Foundation

struct Schools:Codable {
    let success: Bool
    let message:String
    
    let data: [Data]
}

struct Data: Codable{
    let id : Int
    let name:String
    let nation:String
}
